# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
  if $('#text').val().length == 0
    $('#submit').attr 'disabled', 'disabled'
    $('#cancel').attr 'disabled', 'disabled'
  $('#text').bind 'keydown keyup keypress change', ->
    if $(this).val().length > 0
      $('#submit').removeAttr 'disabled'
      $('#cancel').removeAttr 'disabled'
    else
      $('#submit').attr 'disabled', 'disabled'
      $('#cancel').attr 'disabled', 'disabled'
    return
  return