class ArticlesController < ApplicationController

	#記事一覧
	def index
		@articles = Article.all
		@articles = Article.search(params[:search])
	end

	def show
		@articles = Article.all
		@article = Article.find(params[:id])
	end

	def new
		@articles = Article.all
		@article = Article.new
	end

	def edit
		@articles = Article.all
		@article = Article.find(params[:id])
	end

	def create
		@articles = Article.all
		@article = Article.new(article_params)
		if @article.save
			redirect_to articles_path, notice: "新しいお知らせを登録しました。"
		else
			render "new"
		end
	end
	
	def article_params
		params.require(:article).permit(:title, :text, :released_at)
	end
end