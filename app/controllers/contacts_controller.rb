class ContactsController < ApplicationController
    def index
        @contacts = Contact.all
        @articles = Article.all
        @contacts = Contact.search(params[:search])
    end
    
    def new
        @contact = Contact.new
        @articles = Article.all
    end

    def create
        @articles = Article.all
        @contact = Contact.new(contact_params)
        if @contact.save
            redirect_to contacts_path, notice: "講義情報を登録しました。"
        else
            render :new
        end
    end
    
    def contact_params
        params.require(:contact).permit(:article_type, :class_name, :class_place, :remark, :arrival_date, :published_at)
    end
end