class TopsController < ApplicationController
  def index
    @articles = Article.all
    @tops = Top.search(params[:search])
    @contacts = Contact.all
  end

  def show
    @articles = Article.all
  end
  
  def new
    @articles = Article.all
    @top = Top.new
  end
  
  def create
    @articles = Article.all
    @top = Top.new(top_params)
		if @top.save
			redirect_to tops_path, notice: "新しい講義を登録しました。"
		else
			render "new"
		end
  end
  
  def top_params
		params.require(:top).permit(:class_type, :class_name, :class_place, :period, :weekday, :semester)
	end
end
