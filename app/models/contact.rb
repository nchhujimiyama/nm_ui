class Contact < ActiveRecord::Base
    def self.search(search)
        if search
            Contact.where(['article_type LIKE ?', "%#{search}%"])
        else
            Contact.all
        end
    end
end
