class Top < ActiveRecord::Base
    def self.search(search)
        if search
            Top.where(['semester LIKE ?', "#{search}"])
        else
            Top.all
        end
    end
end
