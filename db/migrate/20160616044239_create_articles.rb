class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title
      t.text :text
      t.datetime :released_at, null: false
      t.datetime :expired_at
      t.boolean :checked, :null => false, :default => false

      t.timestamps null: false
    end
  end
end
