class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :article_type
      t.string :class_name
      t.string :class_place
      t.text :remark
      t.date :arrival_date
      t.datetime :published_at

      t.timestamps null: false
    end
  end
end
