class CreateTops < ActiveRecord::Migration
  def change
    create_table :tops do |t|
      t.string :class_name # 授業名
      t.string :class_type # 種類（専門、般教、資格）
      t.string :class_place # 教室
      t.integer :period # 時限
      t.integer :weekday # 曜日
      t.string :semester # 学期

      t.timestamps null: false
    end
  end
end
